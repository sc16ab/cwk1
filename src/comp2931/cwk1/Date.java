// Class for COMP2931 Coursework 1

package comp2931.cwk1;


import java.util.Calendar;


/**
 * Simple representation of a date.
 */
public class Date
{
  private static final int MONTHS_IN_YEAR = 12;
  // Stores the number of days in each of the 12 months
  private static final int[] DAYS_IN_MONTHS = {
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
  };

  private int year;
  private int month;
  private int day;

  /**
   * Creates a date with todays values for year, month and day.
   */
  public Date() {
    Calendar now = Calendar.getInstance();

    year = now.get(Calendar.YEAR);
    month = now.get(Calendar.MONTH);
    day = now.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    if (y < 1) {
      throw new IllegalArgumentException("years out of range");
    }
    if (m < 1 || m > MONTHS_IN_YEAR) {
      throw new IllegalArgumentException("months out of range");
    }
    if (d < 1 || d > getDaysInMonth(m, y)) {
      throw new IllegalArgumentException("days out of range");
    }

    year = y;
    month = m;
    day = d;
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Returns the day of the year as an integer.
   *
   * @return Day of the year
   */
  public int getDayOfYear() {
    int dayOfYear = getDay();
    int y = getYear();

    for (int i = 1; i < getMonth(); i++) {
      dayOfYear += getDaysInMonth(i, y);
    }

    return dayOfYear;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  /**
   * Tests whether this date is equal to another.
   *
   * <p>The two objects are considered equal if both are instances of
   * the Date class <em>and</em> both represent exactly the same
   * date.</p>
   *
   * @return True if this Date object is equal to the other, false otherwise
   */
  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    }
    else if (! (other instanceof Date)) {
      return false;
    }
    else {
      Date otherDate = (Date) other;
      return getYear() == otherDate.getYear()
          && getMonth() == otherDate.getMonth()
          && getDay() == otherDate.getDay();
    }
  }

  // Helper method to return the number of days in the given month for a given year
  // This method handles leap years
  private static final int getDaysInMonth(int m, int y) {
    // Check if the month is february
    if (m == 2) {
      // Check if the year is a leap year
      if ((y % 100 != 0 && y % 4 == 0) || y % 400 == 0) {
        return DAYS_IN_MONTHS[m - 1] + 1;
      }
    }

    return DAYS_IN_MONTHS[m - 1];
  }
}
