package comp2931.cwk1;

import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import java.util.Calendar;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Unit tests for the Date class.
 */
public class DateTest
{
  private static final String YEARS_OUT_OF_RANGE_ERROR = new String("years out of range");
  private static final String MONTHS_OUT_OF_RANGE_ERROR = new String("months out of range");
  private static final String DAYS_OUT_OF_RANGE_ERROR = new String("days out of range");

  private Date christmas;
  private Date yearZero;

  @Before
  public void setUp() {
    christmas = new Date(2017, 12, 25);
    yearZero = new Date(1, 1, 1);
  }

  @Rule
  public ExpectedException error = ExpectedException.none();

  @Test
  public void validDate() {
    new Date(2017, 2, 28);
    new Date(2015, 1, 31);
    new Date(2013, 4, 30);
    new Date(2011, 3, 1);
  }

  @Test
  public void validDateLeapYear() {
    new Date(2016, 2, 29);
  }

  @Test
  public void dateToString() {
    assertThat(yearZero.toString(), is("0001-01- 1"));
    assertThat(christmas.toString(), is("2017-12-25"));
  }

  @Test
  public void yearToLow() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage(YEARS_OUT_OF_RANGE_ERROR);
    new Date(0, 1, 1);
  }

  @Test
  public void monthToHigh() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage(MONTHS_OUT_OF_RANGE_ERROR);
    new Date(2017, 13, 1);
  }

  @Test
  public void monthToLow() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage(MONTHS_OUT_OF_RANGE_ERROR);
    new Date(2017, 0, 1);
  }

  @Test
  public void dayToHigh() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage(DAYS_OUT_OF_RANGE_ERROR);
    new Date(2017, 2, 29);
  }

  @Test
  public void dayToLow() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage(DAYS_OUT_OF_RANGE_ERROR);
    new Date(2017, 2, 0);
  }

  @Test
  public void dayToHighLeapYear() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage(DAYS_OUT_OF_RANGE_ERROR);
    new Date(2016, 2, 30);
  }

  @Test
  public void equality() {
    assertTrue(christmas.equals(christmas));
    assertTrue(christmas.equals(new Date(2017, 12, 25)));
    assertFalse(christmas.equals(new Date(2017, 12, 1)));
    assertFalse(christmas.equals(new Date(2017, 1, 25)));
    assertFalse(christmas.equals(new Date(1, 12, 25)));
    assertFalse(christmas.equals("2017-12-25"));
  }

  @Test
  public void dayOfYear() {
    // Non leap years
    assertThat(christmas.getDayOfYear(), is(359));
    assertThat(new Date(1066, 12, 31).getDayOfYear(), is(365));
    assertThat(yearZero.getDayOfYear(), is(1));
  }

  @Test
  public void dayOfLeapYear() {
    // Leap years
    assertThat(new Date(2016, 12, 31).getDayOfYear(), is(366));
    assertThat(new Date(4000, 3, 24).getDayOfYear(), is(84));
  }

  @Test
  public void todaysDate() {
    Calendar now = Calendar.getInstance();
    Date today = new Date(now.get(Calendar.YEAR),
                      now.get(Calendar.MONTH),
                      now.get(Calendar.DAY_OF_MONTH));
    assertTrue(today.equals(new Date()));
  }
}
